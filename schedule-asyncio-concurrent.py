import concurrent.futures
import asyncio
import time


def work(x):
    return x**2


async def main_coro(loop):
    now = time.time()
    fs = [asyncio.ensure_future(loop.run_in_executor(None, work, x))
          for x in range(10000)]
    acc = 0
    for f in asyncio.as_completed(fs):
        acc += await f
    print(acc)
    print(time.time() - now)


def main():
    loop = asyncio.get_event_loop()
    loop.set_default_executor(concurrent.futures.ProcessPoolExecutor(48))
    loop.run_until_complete(main_coro(loop))


if __name__ == "__main__":
    main()
