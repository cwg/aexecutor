import asyncio
import time
import aexecutor


def work(x):
    return x**2


async def main_coro():
    async with aexecutor.ProcessPoolExecutor(48) as executor:
        now = time.time()
        fs = [executor.submit(work, x) for x in range(10000)]
        acc = 0
        for f in asyncio.as_completed(fs):
            acc += await f
        print(acc)
        print(time.time() - now)


def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_coro())


if __name__ == "__main__":
    main()
