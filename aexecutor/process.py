# Copyright 2009 Brian Quinlan. All Rights Reserved.
# Licensed to PSF under a Contributor Agreement.

# Modifications to use asyncio: Copyright 2016 Christoph Groth.

"""Implements ProcessPoolExecutor.

The follow diagram and text describe the data-flow through the system:

|======================= In-process =====================|== Out-of-process ==|

+----------+     +----------+       +--------+     +-----------+    +---------+
|          |  => | Work Ids |    => |        |  => | Call Q    | => |         |
|          |     +----------+       |        |     +-----------+    |         |
|          |     | ...      |       |        |     | ...       |    |         |
|          |     | 6        |       |        |     | 5, call() |    |         |
|          |     | 7        |       |        |     | ...       |    |         |
| Process  |     | ...      |       | Local  |     +-----------+    | Process |
|  Pool    |     +----------+       | Worker |                      |  #1..n  |
| Executor |                        | Thread |                      |         |
|          |     +----------- +     |        |     +-----------+    |         |
|          | <=> | Work Items | <=> |        | <=  | Result Q  | <= |         |
|          |     +------------+     |        |     +-----------+    |         |
|          |     | 6: call()  |     |        |     | ...       |    |         |
|          |     |    future  |     |        |     | 4, result |    |         |
|          |     | ...        |     |        |     | 3, except |    |         |
+----------+     +------------+     +--------+     +-----------+    +---------+

Executor.submit() called:
- creates a uniquely numbered _WorkItem and adds it to the "Work Items" dict
- adds the id of the _WorkItem to the "Work Ids" queue

Local worker thread:
- reads work ids from the "Work Ids" queue and looks up the corresponding
  WorkItem from the "Work Items" dict: if the work item has been cancelled then
  it is simply removed from the dict, otherwise it is repackaged as a
  _CallItem and put in the "Call Q". New _CallItems are put in the "Call Q"
  until "Call Q" is full. NOTE: the size of the "Call Q" is kept small because
  calls placed in the "Call Q" can no longer be cancelled with Future.cancel().
- reads _ResultItems from "Result Q", updates the future stored in the
  "Work Items" dict and deletes the dict entry

Process #1..n:
- reads _CallItems from "Call Q", executes the calls, and puts the resulting
  _ResultItems in "Result Q"
"""

__author__ = 'Brian Quinlan (brian@sweetapp.com)'

import asyncio
import os
from . import _base
from queue import Full
import multiprocessing
from functools import partial
import itertools
import traceback
from collections import namedtuple

# Controls how many more calls than processes will be queued in the call queue.
# A smaller number will mean that processes spend more time idle waiting for
# work while a larger number will make Future.cancel() succeed less frequently
# (Futures in the call queue cannot be cancelled).
EXTRA_QUEUED_CALLS = 1

# Hack to embed stringification of remote traceback in local traceback

class _RemoteTraceback(Exception):
    def __init__(self, tb):
        self.tb = tb
    def __str__(self):
        return self.tb

class _ExceptionWithTraceback:
    def __init__(self, exc, tb):
        tb = traceback.format_exception(type(exc), exc, tb)
        tb = ''.join(tb)
        self.exc = exc
        self.tb = '\n"""\n%s"""' % tb
    def __reduce__(self):
        return _rebuild_exc, (self.exc, self.tb)

def _rebuild_exc(exc, tb):
    exc.__cause__ = _RemoteTraceback(tb)
    return exc

_WorkItem = namedtuple("_WorkItem", ["future", "fn", "args", "kwargs"])
_ResultItem = namedtuple("_ResultItem", ["work_id", "exception", "result"])
_CallItem = namedtuple("_CallItem", ["work_id", "fn", "args", "kwargs"])

def _get_chunks(*iterables, chunksize):
    """ Iterates over zip()ed iterables in chunks. """
    it = zip(*iterables)
    while True:
        chunk = tuple(itertools.islice(it, chunksize))
        if not chunk:
            return
        yield chunk

def _process_chunk(fn, chunk):
    """ Processes a chunk of an iterable passed to map.

    Runs the function passed to map() on a chunk of the
    iterable passed to map.

    This function is run in a separate process.

    """
    return [fn(*args) for args in chunk]

def _process_worker(call_queue, result_queue):
    """Evaluates calls from call_queue and places the results in result_queue.

    This worker is run in a separate process.

    Args:
        call_queue: A multiprocessing.Queue of _CallItems that will be read and
            evaluated by the worker.
        result_queue: A multiprocessing.Queue of _ResultItems that will written
            to by the worker.
        shutdown: A multiprocessing.Event that will be set as a signal to the
            worker that it should exit when call_queue is empty.
    """
    while True:
        call_item = call_queue.get(block=True)
        if call_item is None:
            return
        try:
            r = call_item.fn(*call_item.args, **call_item.kwargs)
        except BaseException as e:
            exc = _ExceptionWithTraceback(e, e.__traceback__)
            result_queue.put(_ResultItem(call_item.work_id, exc, None))
        else:
            result_queue.put(_ResultItem(call_item.work_id, None, r))

def _add_call_item_to_queue(pending_work_items,
                            work_ids,
                            call_queue):
    """Fills call_queue with _WorkItems from pending_work_items.

    This function never blocks.

    Args:
        pending_work_items: A dict mapping work ids to _WorkItems e.g.
            {5: <_WorkItem...>, 6: <_WorkItem...>, ...}
        work_ids: A queue.Queue of work ids e.g. Queue([5, 6, ...]). Work ids
            are consumed and the corresponding _WorkItems from
            pending_work_items are transformed into _CallItems and put in
            call_queue.
        call_queue: A multiprocessing.Queue that will be filled with _CallItems
            derived from _WorkItems.
    """
    while True:
        if call_queue.full() or work_ids.empty():
            return
        work_id = work_ids.get_nowait()[1]
        work_item = pending_work_items[work_id]

        if not work_item.future.cancelled():
            call_queue.put(_CallItem(work_id,
                                     work_item.fn,
                                     work_item.args,
                                     work_item.kwargs),
                           block=True)
        else:
            del pending_work_items[work_id]
            continue

_system_limits_checked = False
_system_limited = None
def _check_system_limits():
    global _system_limits_checked, _system_limited
    if _system_limits_checked:
        if _system_limited:
            raise NotImplementedError(_system_limited)
    _system_limits_checked = True
    try:
        nsems_max = os.sysconf("SC_SEM_NSEMS_MAX")
    except (AttributeError, ValueError):
        # sysconf not available or setting not available
        return
    if nsems_max == -1:
        # indetermined limit, assume that limit is determined
        # by available memory only
        return
    if nsems_max >= 256:
        # minimum number of semaphores available
        # according to POSIX
        return
    _system_limited = "system provides too few semaphores (%d available, 256 necessary)" % nsems_max
    raise NotImplementedError(_system_limited)


class BrokenProcessPool(RuntimeError):
    """
    Raised when a process in a ProcessPoolExecutor terminated abruptly
    while a future was in the running state.
    """


class ProcessPoolExecutor(_base.Executor):
    def __init__(self, max_workers=None, loop=None,
                 i_will_remember_to_shutdown=False):
        """Initializes a new ProcessPoolExecutor instance.

        Args:
            max_workers: The maximum number of processes that can be used to
                execute the given calls. If None or not given then as many
                worker processes will be created as the machine has processors.
        """
        self._shutdown_promised = i_will_remember_to_shutdown

        _check_system_limits()

        if max_workers is None:
            self._max_workers = os.cpu_count() or 1
        else:
            if max_workers <= 0:
                raise ValueError("max_workers must be greater than 0")

            self._max_workers = max_workers

        # Make the call queue slightly larger than the number of processes to
        # prevent the worker processes from idling. But don't make it too big
        # because futures in the call queue cannot be cancelled.
        self._call_queue = multiprocessing.Queue(self._max_workers +
                                                 EXTRA_QUEUED_CALLS)
        # Killed worker processes can produce spurious "broken pipe"
        # tracebacks in the queue's own worker thread. But we detect killed
        # processes anyway, so silence the tracebacks.
        self._call_queue._ignore_epipe = True
        self._result_queue = multiprocessing.SimpleQueue()
        self._work_ids = asyncio.PriorityQueue()
        self._queue_management_thread = None
        # Map of pids to processes
        self._processes = {}

        # Shutdown is a two-step process.
        self._shutdown_thread = False
        self._broken = False
        self._queue_count = 0
        self._pending_work_items = {}

        self._loop = asyncio.get_event_loop() if loop is None else loop
        self._reader_event = asyncio.Event(loop=self._loop)

    def _terminate_pool(self):
        # Mark the process pool broken so that submits fail right now.
        self._broken = True
        self._shutdown_thread = True
        # All futures in flight must be marked failed
        for work_id, work_item in self._pending_work_items.items():
            work_item.future.set_exception(
                BrokenProcessPool(
                    "A process in the process pool was "
                    "terminated abruptly while the future was "
                    "running or pending."
                ))
            # Delete references to object. See issue16284
            del work_item
        self._pending_work_items.clear()
        # Terminate remaining workers forcibly: the queues or their
        # locks may be in a dirty state and block forever.
        for p in self._processes.values():
            self._loop.remove_reader(p.sentinel)
            p.terminate()

    async def _queue_management_worker(self):
        """Manages the communication between this process and the workers.

        This coroutine is run in the background until the executor is shut down.
        """
        def shutdown_worker():
            # This is an upper bound
            nb_children_alive = sum(p.is_alive()
                                    for p in self._processes.values())
            for i in range(0, nb_children_alive):
                self._call_queue.put_nowait(None)
            # Release the queue's resources as soon as possible.
            self._call_queue.close()
            # If .join() is not called on the created processes then
            # some multiprocessing.Queue methods may deadlock on Mac OS X.
            for p in self._processes.values():
                self._loop.remove_reader(p.sentinel)
                p.join()
            self._loop.remove_reader(reader._handle)

        reader = self._result_queue._reader
        self._loop.add_reader(reader._handle, self._reader_event.set)

        while True:
            _add_call_item_to_queue(self._pending_work_items,
                                    self._work_ids,
                                    self._call_queue)

            # There must be a more efficient way to do this with asyncio.
            # I'm not even sure whether the following loop is 100% correct.
            while True:
                await self._reader_event.wait()
                if reader.poll(0):
                    break
                else:
                    self._reader_event.clear()
            result_item = reader.recv()
            self._reader_event.clear()
            if result_item is not None:
                work_item = self._pending_work_items.pop(result_item.work_id,
                                                         None)
                # work_item can be None if another process terminated (see above)
                if work_item is not None:
                    if not work_item.future.cancelled():
                        if result_item.exception:
                            work_item.future.set_exception(result_item.exception)
                        else:
                            work_item.future.set_result(result_item.result)
                    # Delete references to object. See issue16284
                    del work_item
            # No more work items can be added if the executor that owns this
            # worker has been shutdown.
            if self._shutdown_thread:
                try:
                    # Since no new work items can be added, it is safe to
                    # shutdown this thread if there are no pending work items.
                    if not self._pending_work_items:
                        shutdown_worker()
                        return
                except Full:
                    # This is not a problem: we will eventually be woken up (in
                    # self._result_queue.get()) and be able to send a sentinel
                    # again.
                    pass

    def _start_queue_management_thread(self):
        if not self._shutdown_promised:
            raise RuntimeError(
                'Please initialize the executor in an "async with" statement.\n'
                'Alternatively, set the executor '
                'option i_will_remember_to_shutdown to True.')

        if self._queue_management_thread is None:
            # Start the processes so that their sentinels are known.
            self._adjust_process_count()
            self._queue_management_thread = asyncio.ensure_future(
                self._queue_management_worker(),
                loop=self._loop)

    def _adjust_process_count(self):
        for _ in range(len(self._processes), self._max_workers):
            p = multiprocessing.Process(
                    target=_process_worker,
                    args=(self._call_queue,
                          self._result_queue))
            p.start()
            self._processes[p.pid] = p
            self._loop.add_reader(p.sentinel, self._terminate_pool)

    def prioritize(self, priority, fn, *args, **kwargs):
        if self._broken:
            raise BrokenProcessPool('A child process terminated '
                'abruptly, the process pool is not usable anymore')
        if self._shutdown_thread:
            raise RuntimeError('cannot schedule new futures after shutdown')

        f = asyncio.Future()
        w = _WorkItem(f, fn, args, kwargs)

        self._pending_work_items[self._queue_count] = w
        self._work_ids.put_nowait((priority, self._queue_count))
        self._queue_count += 1

        # Wake up queue management thread.  We use the reader event to avoid
        # overloading the result queue.
        if not self._reader_event.is_set():
            self._reader_event.set()
            self._result_queue.put(None)

        self._start_queue_management_thread()

        return f

    def submit(self, fn, *args, **kwargs):
        return self.prioritize(0, fn, *args, **kwargs)
    submit.__doc__ = _base.Executor.submit.__doc__

    def map(self, fn, *iterables, timeout=None, chunksize=1):
        """Returns an iterator equivalent to map(fn, iter).

        Args:
            fn: A callable that will take as many arguments as there are
                passed iterables.
            timeout: The maximum number of seconds to wait. If None, then there
                is no limit on the wait time.
            chunksize: If greater than one, the iterables will be chopped into
                chunks of size chunksize and submitted to the process pool.
                If set to one, the items in the list will be sent one at a time.

        Returns:
            An iterator equivalent to: map(func, *iterables) but the calls may
            be evaluated out-of-order.

        Raises:
            TimeoutError: If the entire result iterator could not be generated
                before the given timeout.
            Exception: If fn(*args) raises for any values.
        """
        if chunksize < 1:
            raise ValueError("chunksize must be >= 1.")

        results = super().map(partial(_process_chunk, fn),
                              _get_chunks(*iterables, chunksize=chunksize),
                              timeout=timeout)
        return itertools.chain.from_iterable(results)

    async def shutdown(self, wait=True):
        self._shutdown_thread = True
        if self._queue_management_thread:
            # Wake up queue management thread
            self._result_queue.put(None)
            if wait:
                await asyncio.wait_for(self._queue_management_thread, None)
        # To reduce the risk of opening too many files, remove references to
        # objects that use file descriptors.
        self._queue_management_thread = None
        self._call_queue = None
        self._result_queue = None
        self._processes = None
    shutdown.__doc__ = _base.Executor.shutdown.__doc__
