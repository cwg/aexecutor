from concurrent import futures
import time


def work(x):
    return x**2


def main():
    executor = futures.ProcessPoolExecutor(48)

    now = time.time()
    fs = [executor.submit(work, x) for x in range(10000)]
    print(sum(f.result() for f in futures.as_completed(fs)))
    print(time.time() - now)


if __name__ == "__main__":
    main()
